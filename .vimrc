"Preload Vim sensible
runtime! plugin/sensible.vim
"Make VIM modern only!
set nocompatible

"Let their be line numbers!
set number


"Syntax!
syntax on
"Move the horrible esc key to jj imap jj <Esc>
imap jj <Esc>
"Auto reload .vimrc after change!
autocmd! bufwritepost .vimrc source %

"Auto Filetype detection (for syntax highlighting etc)
filetype on

"Vundle
set rtp+=~/.vim/bundle/Vundle.vim
let g:vundle_default_git_proto = "ssh"

"Custom mapleader
let mapleader=","

filetype plugin indent on
call vundle#begin()
"Let Vundle Manage Vundle
Bundle 'git@github.com:gmarik/vundle'

"Bundles
Bundle 'git@github.com:kien/ctrlp.vim'
"Bundle 'scrooloose/syntastic'
"Bundle 'Shougo/neocomplcache.vim'
"Bundle 'Valloric/YouCompleteMe'
Bundle 'git@github.com:tpope/vim-surround'
"Bundle 'tomasr/molokai'
"Bundle 'sentientmachine/erics_vim_syntax_and_color_highlighting'
"Bundle 'altercation/vim-colors-solarized'
Bundle 'git@github.com:godlygeek/tabular'
Bundle 'git@github.com:flazz/vim-colorschemes'
Bundle 'git@github.com:vim-scripts/taglist.vim'
Bundle 'git@github.com:ervandew/supertab'
Bundle 'git@github.com:ervandew/snipmate.vim'
"Bundle 'honza/vim-snippets'
"Bundle 'SirVer/ultisnips'
Bundle 'git@github.com:tpope/vim-sensible'
call vundle#end()

"Ultisnips
let g:UltiSnipsExpandTrigger="<c-s>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
"Ctrlp mapping
let g:ctrlp_map = '<c-p>'
let g:ctrp_cmd = 'CtrlPMixed'

"Terminal colour pallete
set t_Co=256

"let g:solarized_termcolors=256
"Molokai Setup
syntax enable
set t_Co=256
set background=dark
colorscheme solarized
set formatoptions+=t
set wrap
set t_ut=
"Set a textwrapping width
"set tw=80

"Auto indenting
set autoindent
set smartindent

"Copy indentation
set copyindent

"Tab
set shiftwidth=4

"Show search matches
set showmatch

"Ignore case for search
set ignorecase

"Auto detect case for search
set smartcase

"Highlight search results
set hlsearch

"Modify terminal titlebar
set title

"Get rid of annoying backup files (never use em' anyway)
set nobackup
set noswapfile

"Neo complete setup:

"let g:acp_enableAtStartup = 0
"let g:neocomplcache_enable_at_startup = 1
"let g:neocomplcache_enable_smart_case=1
"let g:neocomplcache_dictionary_filetype_lists = {
	\ 'default' : '',
	\ 'vimshell' : $HOME.'/.vimshell_hist',
	\ 'scheme' : $HOME.'/.gosh_completions'
	\ }
"inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

"Enable omni
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags


autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

set wildignore+=*.class

" Tab spacing
set ts=4
set sw=4

set tabstop=4
set shiftwidth=4
set expandtab

nnoremap <silent> <Leader>tl :TlistToggle<CR>
map <F7> mzgg=G`z<CR>

map <,f> *``

"Tag List setup
nnoremap <silent> <F8> :TlistToggle<CR>

" Split navigation
" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-k> :wincmd k<CR>                                                          
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-l> :wincmd l<CR>
nmap <silent> <c-h> :wincmd h<CR>
